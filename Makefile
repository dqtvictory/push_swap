LIBPS_DIR	= libps
LIBPS_FNAME	= libps.a

CHECKER_SRC	= checker/main.c
PS_SRC		= push_swap/actions.c push_swap/big_sort.c push_swap/main.c push_swap/search.c push_swap/small_sort.c

CHECKER_OBS	= ${CHECKER_SRC:.c=.o}
PS_OBS		= ${PS_SRC:.c=.o}

CHECKER_BIN	= bin/checker
PS_BIN		= bin/push_swap

CC			= clang
CFLAGS		= -Wall -Wextra -Werror

RM			= rm -f

.c.o:
			${CC} ${CFLAGS} -c $< -o ${<:.c=.o}

all:		push_swap checker

checker:	${CHECKER_OBS}
			make -C ${LIBPS_DIR}
			${CC} ${CFLAGS} -o ${CHECKER_BIN} ${CHECKER_OBS} -L${LIBPS_DIR} -lps

push_swap:	${PS_OBS}
			make -C ${LIBPS_DIR}
			${CC} ${CFLAGS} -o ${PS_BIN} ${PS_OBS} -L${LIBPS_DIR} -lps

clean:
			make -C ${LIBPS_DIR} clean
			${RM} ${CHECKER_OBS} ${PS_OBS}

fclean:		clean
			make -C ${LIBPS_DIR} fclean
			${RM} ${CHECKER_BIN} ${PS_BIN}

re:			fclean all

.PHONY:		all checker push_swap clean fclean re
