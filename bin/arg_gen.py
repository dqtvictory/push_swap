from sys import argv
from random import choice

if __name__ == '__main__':
    
    if len(argv) != 2:
        print("Must have only 1 arg")
        exit(1)
    
    try:
        n = int(argv[1])
        if n not in range(3, 501):
            raise Exception()
    except:
        print("Arg must be an integer between 3 and 500")
        exit(1)
    
    l = list(range(n))
    for i in range(n):
        num = l.pop(choice(range(n - i)))
        if i != n - 1:
            print(num, end=' ')
        else:
            print(num)