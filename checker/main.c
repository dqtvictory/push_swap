/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 21:58:54 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 19:36:04 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libps/libps.h"

static int	error(void)
{
	ft_putendl_fd("Error", STDERR_FILENO);
	return (1);
}

static void	print_results(t_psdata *data)
{
	if (is_sorted(data))
		ft_putendl_fd("OK", STDOUT_FILENO);
	else
		ft_putendl_fd("K0", STDOUT_FILENO);
}

static bool	parse_line(t_psdata *d, char *line)
{
	if (!ft_strncmp(line, "sa", 3))
		return (swap(d, &d->atop));
	if (!ft_strncmp(line, "sb", 3))
		return (swap(d, &d->btop));
	if (!ft_strncmp(line, "ss", 3))
		return (swap(d, &d->atop) && swap(d, &d->btop));
	if (!ft_strncmp(line, "pa", 3))
		return (push(d, &d->atop, &d->btop));
	if (!ft_strncmp(line, "pb", 3))
		return (push(d, &d->btop, &d->atop));
	if (!ft_strncmp(line, "ra", 3))
		return (rotate(d, &d->atop, false));
	if (!ft_strncmp(line, "rb", 3))
		return (rotate(d, &d->btop, false));
	if (!ft_strncmp(line, "rr", 3))
		return (rotate(d, &d->atop, false) && rotate(d, &d->btop, false));
	if (!ft_strncmp(line, "rra", 4))
		return (rotate(d, &d->atop, true));
	if (!ft_strncmp(line, "rrb", 4))
		return (rotate(d, &d->btop, true));
	if (!ft_strncmp(line, "rrr", 4))
		return (rotate(d, &d->atop, true) && rotate(d, &d->btop, true));
	return (!ft_strncmp(line, "", 1));
}

int	main(int ac, char **av)
{
	t_psdata	data;
	char		*line;
	int			gnl;

	if (ac == 1)
		return (0);
	if (!check_args(ac - 1, av + 1) || !init_data(ac - 1, av + 1, &data))
		return (error());
	while (1)
	{
		line = NULL;
		gnl = get_next_line(STDIN_FILENO, &line);
		if (!parse_line(&data, line))
			gnl = -1;
		if (line)
			free(line);
		if (gnl <= 0)
			break ;
	}
	if (!gnl)
		print_results(&data);
	free(data.stack);
	if (gnl)
		return (error());
	return (0);
}
