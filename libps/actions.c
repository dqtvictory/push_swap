/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/31 13:30:17 by qdam              #+#    #+#             */
/*   Updated: 2021/06/08 02:23:04 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libps.h"

static t_stack	*pop(t_stack **top)
{
	t_stack	*elem;

	if (*top == NULL)
		return (NULL);
	elem = *top;
	if (elem->next == NULL)
		*top = NULL;
	else if (elem->next == elem->prev)
	{
		*top = elem->next;
		(*top)->next = NULL;
		(*top)->prev = NULL;
	}
	else
	{
		elem->prev->next = elem->next;
		elem->next->prev = elem->prev;
		*top = elem->next;
	}
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}

static t_stack	*insert(t_stack **top, t_stack *elem)
{
	if (*top && (*top)->next == NULL)
	{
		(*top)->next = elem;
		(*top)->prev = elem;
		elem->next = (*top);
		elem->prev = (*top);
	}
	else if (*top && (*top)->next)
	{
		(*top)->prev->next = elem;
		elem->prev = (*top)->prev;
		elem->next = *top;
		(*top)->prev = elem;
	}
	*top = elem;
	return (*top);
}

bool	swap(t_psdata *data, t_stack **top)
{
	int	v;

	(void)data;
	if (*top)
	{
		v = (*top)->face;
		(*top)->face = (*top)->next->face;
		(*top)->next->face = v;
	}
	return (true);
}

bool	push(t_psdata *data, t_stack **to_top, t_stack **from_top)
{
	t_stack	*elem;

	(void)data;
	if (*from_top)
	{
		elem = pop(from_top);
		insert(to_top, elem);
	}
	return (true);
}

bool	rotate(t_psdata *data, t_stack **top, bool rev)
{
	(void)data;
	if (*top && (*top)->next)
	{
		if (rev)
			*top = (*top)->prev;
		else
			*top = (*top)->next;
	}
	return (true);
}
