/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 23:00:07 by qdam              #+#    #+#             */
/*   Updated: 2021/06/14 16:49:52 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libps.h"

static bool	check_single_arg(char *arg)
{
	long	sign;
	long	val;

	if (!ft_strlen(arg) || ft_strlen(arg) > 11)
		return (false);
	sign = 1;
	if (*arg == '-')
		sign = -1 * (arg++ != NULL);
	if (sign == -1 && !(*arg))
		return (false);
	val = 0;
	while (*arg)
	{
		if (!ft_isdigit(*arg))
			return (false);
		val = (*arg - '0') + val * 10;
		arg++;
	}
	val *= sign;
	if (val < MINI || val > MAXI)
		return (false);
	return (true);
}

static bool	check_duplicates(int ac, char **av)
{
	int	i;
	int	j;

	i = 0;
	while (i < ac - 1)
	{
		j = i + 1;
		while (j < ac)
		{
			if (!ft_strncmp(av[i], av[j], 11))
				return (false);
			j++;
		}
		i++;
	}
	return (true);
}

bool	check_args(int ac, char **av)
{
	int		i;

	i = -1;
	while (++i < ac)
		if (!check_single_arg(av[i]))
			return (false);
	if (!check_duplicates(ac, av))
		return (false);
	return (true);
}
