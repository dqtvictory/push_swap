/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/09 12:34:46 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 19:15:00 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libps.h"

static char	*join(char const *s1, char const *s2)
{
	size_t	len1;
	size_t	len2;
	char	*join;

	if (!s1 || !s2)
		return (0);
	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	join = malloc(sizeof(char) * (len1 + len2 + 1));
	if (join)
	{
		ft_memcpy(join, s1, len1);
		ft_memcpy(join + len1, s2, len2);
		join[len1 + len2] = 0;
	}
	return (join);
}

char	*ft_free(char *ret_value, char *ptr1, char *ptr2)
{
	if (ptr1)
	{
		free(ptr1);
		ptr1 = NULL;
	}
	if (ptr2)
	{
		free(ptr2);
		ptr2 = NULL;
	}
	return (ret_value);
}

void	join_str(char **tmp, char **str, char *buf)
{
	*tmp = *str;
	*str = join(*str, buf);
	free(*tmp);
}

char	*read_line(int fd, char *str)
{
	char	*buf;
	char	*tmp;
	int		ret;

	buf = malloc(sizeof(char) * BFSZ + 1);
	if (!buf || fd < 0 || BFSZ <= 0)
		return (ft_free(NULL, buf, NULL));
	if (!str)
		str = ft_strdup("");
	while (!(ft_strchr(str, '\n')))
	{
		ret = read(fd, buf, BFSZ);
		if (ret < 0)
			return (ft_free(NULL, buf, NULL));
		buf[ret] = '\0';
		join_str(&tmp, &str, buf);
		if (ret == 0)
			break ;
	}
	return (ft_free(str, buf, NULL));
}

int	get_next_line(int fd, char **line)
{
	static char	*str;
	char		*index;
	char		*tmp;

	if (read(fd, NULL, 0) == -1)
		return (-1);
	str = read_line(fd, str);
	if (fd < 0 || !line || !str)
		return (-1);
	index = ft_strchr(str, '\n');
	if (index)
	{
		tmp = str;
		*line = ft_substr(str, 0, index - str);
		str = ft_substr(str, (index - str) + 1, ft_strlen(str));
		free(tmp);
		return (1);
	}
	else
	{
		*line = ft_strdup(str);
		free(str);
		str = NULL;
		return (0);
	}
}
