/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 19:43:10 by qdam              #+#    #+#             */
/*   Updated: 2021/04/15 19:43:10 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	join(char **strs, size_t size, char *big_str, char sep)
{
	size_t	i;
	size_t	j;
	size_t	l;

	i = 0;
	j = 0;
	while (j < size)
	{
		l = ft_strlen(strs[j]);
		ft_memcpy(big_str + i, strs[j], l);
		if (j < size - 1)
			*(big_str + i + l) = sep;
		i += l + 1;
		j++;
	}
}

char	*ft_strjoin(size_t size, char **strs, char sep)
{
	char	*big_str;
	size_t	i;
	size_t	big_len;

	big_len = 0;
	if (size > 0)
	{
		i = 0;
		while (i < size)
		{
			big_len += ft_strlen(strs[i]);
			i++;
		}
		big_len += size - 1;
	}
	big_str = malloc(sizeof(char) * (big_len + 1));
	if (big_str)
	{
		big_str[big_len] = 0;
		join(strs, size, big_str, sep);
	}
	return (big_str);
}
