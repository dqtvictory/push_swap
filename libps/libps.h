/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libps.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 20:32:10 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 16:12:18 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBPS_H
# define LIBPS_H

# define MINI -2147483648
# define MAXI 2147483647
# define BFSZ 1024

# include "libft/libft.h"
# include <stdio.h>

typedef enum e_action
{
	NONE = 0,
	SA = 1,
	SB = 2,
	SS = 3,
	RA = 4,
	RB = 5,
	RR = 6,
	RRA = 7,
	RRB = 8,
	RRR = 9,
	PA = 10,
	PB = 11
}	t_action;

typedef struct s_stack
{
	int				face;
	unsigned int	val;
	struct s_stack	*next;
	struct s_stack	*prev;
}	t_stack;

typedef struct s_psdata
{
	int			count;
	t_stack		*stack;
	t_stack		*atop;
	t_stack		*btop;
}	t_psdata;

void	free_ptr_tab(void **arr);
void	sort_int_tab(int *tab, int size);
int		index_of(int num, int *tab, int size);
int		get_next_line(int fd, char **line);

bool	check_args(int ac, char **av);
bool	init_data(int ac, char **av, t_psdata *data);
bool	is_sorted(t_psdata *data);

bool	swap(t_psdata *data, t_stack **top);
bool	push(t_psdata *data, t_stack **to_top, t_stack **from_top);
bool	rotate(t_psdata *data, t_stack **top, bool rev);

#endif
