/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/29 18:33:55 by qdam              #+#    #+#             */
/*   Updated: 2021/06/14 16:48:05 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libps.h"

static t_stack	*alloc_stack(int ac, char **av)
{
	t_stack	*stack;
	int		i;

	stack = malloc(sizeof(t_stack) * ac);
	if (!stack)
		return (NULL);
	i = -1;
	while (++i < ac)
	{
		stack[i].face = ft_atoi(av[i]);
		if (i == 0)
			stack[i].prev = &stack[ac - 1];
		else
			stack[i].prev = &stack[i - 1];
		if (i < ac - 1)
			stack[i].next = &stack[i + 1];
		else
			stack[i].next = &stack[0];
	}
	if (i == 1)
	{
		stack->next = NULL;
		stack->prev = NULL;
	}
	return (stack);
}

static bool	assign_val(int count, t_stack *stack)
{
	int	*tab;
	int	i;

	tab = malloc(sizeof(int) * count);
	if (!tab)
		return (false);
	i = -1;
	while (++i < count)
	{
		tab[i] = stack->face;
		stack = stack->next;
	}
	sort_int_tab(tab, count);
	i = -1;
	while (++i < count)
	{
		stack->val = index_of(stack->face, tab, count);
		stack = stack->next;
	}
	free(tab);
	return (true);
}

bool	is_sorted(t_psdata *data)
{
	t_stack	*cur;

	if (data->btop)
		return (false);
	cur = data->atop->next;
	while (cur != data->atop)
	{
		if (cur->face < cur->prev->face)
			return (false);
		cur = cur->next;
	}
	return (true);
}

bool	init_data(int ac, char **av, t_psdata *data)
{
	data->stack = alloc_stack(ac, av);
	if (!data->stack)
		return (NULL);
	data->atop = data->stack;
	data->btop = NULL;
	data->count = ac;
	if (data->count == 1)
		return (true);
	if (!is_sorted(data) && !assign_val(ac, data->stack))
	{
		free(data->stack);
		return (false);
	}
	return (true);
}
