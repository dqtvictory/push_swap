/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 23:11:06 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 14:26:00 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libps.h"

int	index_of(int num, int *tab, int size)
{
	int	i;

	i = -1;
	while (++i < size)
		if (tab[i] == num)
			return (i);
	return (-1);
}

void	sort_int_tab(int *tab, int size)
{
	int	i;
	int	p;
	int	temp;

	i = 1;
	while (i < size)
	{
		p = i;
		while (p > 0 && tab[p] < tab[p - 1])
		{
			temp = tab[p];
			tab[p] = tab[p - 1];
			tab[p - 1] = temp;
			p--;
		}
		i++;
	}
}

void	free_ptr_tab(void **arr)
{
	size_t	i;

	i = 0;
	while (arr[i])
		free(arr[i++]);
	free(arr);
}
