/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/04 23:36:58 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/09 19:18:48 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libps/libps.h"

static void	do_swap(t_psdata *data, t_action act)
{
	if (act == SA)
		swap(data, &data->atop);
	else if (act == SB)
		swap(data, &data->btop);
	else if (act == SS)
	{
		swap(data, &data->atop);
		swap(data, &data->btop);
	}
}

static void	do_rotate(t_psdata *data, t_action act)
{
	if (act == RA)
		rotate(data, &data->atop, false);
	else if (act == RB)
		rotate(data, &data->btop, false);
	else if (act == RR)
	{
		rotate(data, &data->atop, false);
		rotate(data, &data->btop, false);
	}
}

static void	do_rev_rotate(t_psdata *data, t_action act)
{
	if (act == RRA)
		rotate(data, &data->atop, true);
	else if (act == RRB)
		rotate(data, &data->btop, true);
	else if (act == RRR)
	{
		rotate(data, &data->atop, true);
		rotate(data, &data->btop, true);
	}
}

static void	do_push(t_psdata *data, t_action act)
{
	if (act == PA)
		push(data, &data->atop, &data->btop);
	else if (act == PB)
		push(data, &data->btop, &data->atop);
}

bool	do_action(t_psdata *data, t_action act)
{
	static char	*actions[] = {NULL, "sa", "sb", "ss", "ra", "rb", "rr",
								"rra", "rrb", "rrr", "pa", "pb"};

	if (act >= SA && act <= SS)
		do_swap(data, act);
	else if (act >= RA && act <= RR)
		do_rotate(data, act);
	else if (act >= RRA && act <= RRR)
		do_rev_rotate(data, act);
	else if (act == PA || act == PB)
		do_push(data, act);
	ft_putendl_fd(actions[act], STDOUT_FILENO);
	return (is_sorted(data));
}
