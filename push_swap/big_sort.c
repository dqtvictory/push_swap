/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/08 01:05:04 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/22 01:11:00 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static bool	repeat(t_psdata *data, t_action act, int count)
{
	while (count--)
		if (do_action(data, act))
			return (true);
	return (false);
}

static bool	loop_mask(t_psdata *data, int bit_mask)
{
	int		count_a;
	int		i;
	t_stack	*first_one;

	count_a = data->count + 1;
	first_one = first_bit_one_mask(data, bit_mask);
	while (--count_a)
	{
		i = idx_val_mask(data->atop, count_a, bit_mask, 0);
		if (i == MAXI)
			break ;
		if ((i >= 0 && repeat(data, RA, i))
			|| (i < 0 && repeat(data, RRA, -i)))
			return (true);
		do_action(data, PB);
	}
	if (first_one)
	{
		i = idx_cmp(data->atop, count_a, first_one);
		if ((i >= 0 && repeat(data, RA, i))
			|| (i < 0 && repeat(data, RRA, -i)))
			return (true);
	}
	repeat(data, PA, data->count - count_a);
	return (false);
}

void	big_sort(t_psdata *data)
{
	int		bit_mask;

	bit_mask = 1;
	while (bit_mask <= data->count - 1)
	{
		if (loop_mask(data, bit_mask))
			return ;
		bit_mask <<= 1;
	}
}
