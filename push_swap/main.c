/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/04 23:33:41 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/14 16:44:18 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	error(void)
{
	ft_putendl_fd("Error", STDERR_FILENO);
	return (1);
}

int	main(int ac, char **av)
{
	t_psdata	data;

	if (ac == 1)
		return (0);
	if (!check_args(ac - 1, av + 1) || !init_data(ac - 1, av + 1, &data))
		return (error());
	if (data.count > 1 && !is_sorted(&data))
	{
		if (data.count <= 5)
			small_sort(&data);
		else
			big_sort(&data);
	}
	free(data.stack);
}
