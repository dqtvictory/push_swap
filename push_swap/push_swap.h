/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/09 14:08:23 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 16:40:39 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libps/libps.h"

t_stack	*first_bit_one_mask(t_psdata *data, int bit_mask);
int		idx_cmp(t_stack *stack, int size, t_stack *ref);
int		idx_val_mask(t_stack *stack, int size, int bit_mask, unsigned int val);

void	small_sort(t_psdata *data);
void	big_sort(t_psdata *data);

bool	do_action(t_psdata *data, t_action act);

#endif
