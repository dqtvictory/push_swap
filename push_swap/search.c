/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/09 14:05:10 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 16:40:25 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_stack	*first_bit_one_mask(t_psdata *data, int bit_mask)
{
	t_stack	*top;

	top = data->atop;
	while (1)
	{
		if (top->val & bit_mask)
			return (top);
		top = top->next;
		if (top == data->atop)
			return (NULL);
	}
}

int	idx_cmp(t_stack *stack, int size, t_stack *ref)
{
	int		i;
	t_stack	*s;

	i = -1;
	s = stack;
	while (++i < size)
	{
		if (s == ref)
			break ;
		s = s->next;
	}
	if (i == size)
		return (MAXI);
	if (i <= size / 2)
		return (i);
	return (i - size);
}

int	idx_val_mask(t_stack *stack, int size, int bit_mask, unsigned int val)
{
	int		i;
	t_stack	*s;

	i = -1;
	s = stack;
	while (++i < size)
	{
		if ((s->val & bit_mask) == val)
			break ;
		s = s->next;
	}
	if (i == size)
		return (MAXI);
	if (i <= size / 2)
		return (i);
	return (i - size);
}
