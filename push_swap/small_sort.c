/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/07 11:56:17 by qdam              #+#    #+#             */
/*   Updated: 2021/06/09 16:21:33 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sort_two(t_psdata *data)
{
	if (!is_sorted(data))
		do_action(data, SA);
}

static void	sort_three(t_psdata *data)
{
	int	tab[3];

	if (is_sorted(data))
		return ;
	tab[0] = data->atop->val;
	tab[1] = data->atop->next->val;
	tab[2] = data->atop->prev->val;
	if (tab[1] < tab[0] && tab[0] < tab[2])
		do_action(data, SA);
	else if (tab[2] < tab[1] && tab[1] < tab[0])
		(void)(do_action(data, SA) + do_action(data, RRA));
	else if (tab[1] < tab[2] && tab[2] < tab[0])
		do_action(data, RA);
	else if (tab[0] < tab[2] && tab[2] < tab[1])
		(void)(do_action(data, SA) + do_action(data, RA));
	else if (tab[2] < tab[0] && tab[0] < tab[1])
		do_action(data, RRA);
}

static void	sort_four(t_psdata *data)
{
	int	tab[4];
	int	min;
	int	i;

	tab[0] = data->atop->val;
	tab[1] = data->atop->next->val;
	tab[2] = data->atop->next->next->val;
	tab[3] = data->atop->next->next->next->val;
	min = MAXI;
	i = -1;
	while (++i < 4)
		min = min * (tab[i] >= min) + tab[i] * (tab[i] < min);
	i = index_of(min, tab, 4);
	if (i == 1)
		i = do_action(data, RA);
	else if (i == 2)
		i = do_action(data, RA) + do_action(data, RA);
	else if (i == 3)
		i = do_action(data, RRA);
	do_action(data, PB);
	sort_three(data);
	do_action(data, PA);
}

static void	sort_five(t_psdata *data)
{
	int	tab[5];
	int	min;
	int	i;

	tab[0] = data->atop->val;
	tab[1] = data->atop->next->val;
	tab[2] = data->atop->next->next->val;
	tab[3] = data->atop->next->next->next->val;
	tab[4] = data->atop->next->next->next->next->val;
	min = MAXI;
	i = -1;
	while (++i < 5)
		min = min * (tab[i] >= min) + tab[i] * (tab[i] < min);
	i = index_of(min, tab, 5);
	if (i == 1)
		i = do_action(data, RA);
	else if (i == 2)
		i = do_action(data, RA) + do_action(data, RA);
	else if (i == 3)
		i = do_action(data, RRA) + do_action(data, RRA);
	else if (i == 4)
		i = do_action(data, RRA);
	do_action(data, PB);
	sort_four(data);
	do_action(data, PA);
}

void	small_sort(t_psdata *data)
{
	if (data->count == 2)
		sort_two(data);
	else if (data->count == 3)
		sort_three(data);
	else if (data->count == 4)
		sort_four(data);
	else
		sort_five(data);
}
